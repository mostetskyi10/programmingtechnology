﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Lab8
{
    class Program
    {
        static void Main(string[] args)
        {
            var rand = new Random();
            var min = 5000;
            var max = 8000;  
            var lenght = (int)Math.Sqrt(rand.Next(min,max));
            var textFileName = "Graph.txt";
            var graph = new List<List<Node>> ();
       
            var result = 0.0;
            Console.WriteLine("Your graph size is {0}x{0}", lenght);
            Console.Write("Input (i): ");
            var targetI = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input (j): ");
            var targetJ = Convert.ToInt32(Console.ReadLine());


            for (int i = 0; i < lenght; i++)
            {
                graph.Add(new List<Node>());
                for (int j = 0; j < lenght; j++)
                {
                    var node = new Node()
                    {
                        Row = i,
                        Column = j,
                        Value = rand.Next(1, 100)
                    };

                    graph[i].Add(node);
                }

                
            }

            System.IO.File.WriteAllText(textFileName, string.Empty);

            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(textFileName))
            {
                foreach (var row in graph)
                {
                    foreach (var node in row)
                    {
                        file.WriteLine("{0} ",  node);
                    }
                }
            }

            Parallel.ForEach(graph, row=> {
                Parallel.ForEach(row, node =>
                 {
                     if (node.Row == targetI && node.Column == targetJ)
                     {
                         result = node.Value;
                     }
                 });
            });

            Console.WriteLine("From vertex {0} to vertex {1} path costs: {2} ",targetI,targetJ,result);
        }
    }




    class Node
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public int Value { get; set; }

        public override string ToString()
        {
            return string.Format("From {0}, To {1}, Value: {2}", Row, Column, Value);
        }
    }
}