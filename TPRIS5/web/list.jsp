<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>jQuery, Ajax and Servlet/JSP integration example</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"
            type="text/javascript"></script>
    <script src="https://www.gstatic.com/firebasejs/5.0.1/firebase.js"></script>
    <script type="text/javascript">
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyBsLOcrPVHXiw8ggMPzV0W26eQca4MuHIs",
            authDomain: "my-lab2-f6420.firebaseapp.com",
            databaseURL: "https://my-lab2-f6420.firebaseio.com",
            projectId: "my-lab2-f6420",
            storageBucket: "my-lab2-f6420.appspot.com",
            messagingSenderId: "99180894813"
        };
        firebase.initializeApp(config);
    </script>
</head>
<body>
<div style="float: left;width: 50%">
    <form>
        <div class="form-group">
            Element: <input type="text" id="element"/>
        </div>
        <input type="button" onclick="pushData()" value="Push">
        <input type="button" onclick="popData()" value="Pop">
    </form>
    <br>
    <br>


    <strong>Logs:</strong>
    <div id="logs">

    </div>


    <strong>Operation:</strong>
    <div id="ajaxGetUserServletResponse"></div>
</div>

<div style="float: right; width: 50%;">
    <form>
        <div class="form-group">
            Element: <input type="text" id="split"/>
        </div>
        <input type="button" onclick="splitTree()" value="Split">
    </form>
    <br>
    <br>
    <strong>Logs:</strong>
    <div id="logsTree">
    </div>
    <script>


        $(document).ready(function () {
            $('#userName').blur(function (event) {
                var name = $('#userName').val();
                $.get('GetUserServlet', {
                    userName: name
                }, function (responseText) {
                    $('#ajaxGetUserServletResponse').text(responseText);
                });
            });
        });

        function sayHello() {
            alert("Hello")
        }

        var i = 0;

        function loadLogs() {
            var databaseRef = firebase.database().ref('list/');
            var count = 0;
            var res = ""
            databaseRef.once('value', function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    var key = childSnapshot.key;
                    res += childData + ",";

                    $("#logs").append("<br/><label>Index =" + key + " Value=" + childData + "</label>");
                    count++;
                });
                $.get('UploadList', {
                    data: res
                });


                i = count;
                console.log(i)
            });
        }

        function pushData() {
            var element = $('#element').val();
            $.get('InsertIntoList', {
                element: element
            }, function (responseText) {
                var uid = firebase.database().ref().child('list').push().key;
                var updates = {};
                updates['/list/' + i] = element;
                firebase.database().ref().update(updates);
                $('#logs').append(("<br/><label>Index = " + i + " Value = " + element + "</label>"))

                // $('#ajaxGetUserServletResponse').text(responseText);
                i++;

            });
//        window.location.reload();

        }

        function splitTree() {
            var element = $('#split').val();

            $.get('SplitTree',
                {split: element},
                function (responseText) {
                    var treeArray = JSON.parse(responseText);
                    var firstTree = treeArray[0];
                    var secondTree = treeArray[1];
                    console.log(firstTree + " sss " + secondTree)

                    var uid = firebase.database().ref().child('tree').push().key;
                    var updates = {};
                    updates['/tree/'] = treeArray;
                    // i++;
                    firebase.database().ref().update(updates);
                    $('#logsTree').text("Value: " + responseText);
                });
        }


        /*function popData() {
            $.get('GetFromList', function (responseText) {

                //var job=responseText.substring(responseText.indexOf(":")+1,responseText.indexOf("P")).trim();
                for(var index =i;index>=0;index--) {
                    if(index%2===0)
                    firebase.database().ref().child('/list/' + index).remove();

                }

                //  $('#logs').append(("<br/><label>Value added: Priority = " + priority + " Value = " + job + "</label>"))
                $('#ajaxGetUserServletResponse').text("Value: " + responseText);
            });
            //window.location.reload();
        }*/


        function popData() {
            $.get('GetFromList', function (responseText) {
                firebase.database().ref().child('/list/').remove();
                var list = responseText.split(",")
                list.forEach(function (item, i, list) {
                    var uid = firebase.database().ref().child('list').push().key;
                    var updates = {};
                    updates['/list/' + i] = item.trim();
                    // i++;
                    firebase.database().ref().update(updates);
                });
                i = Math.ceil(i / 2);

                //  $('#logs').append(("<br/><label>Value added: Priority = " + priority + " Value = " + job + "</label>"))
                $('#ajaxGetUserServletResponse').text("Value: " + responseText);
                window.location.reload();
            });
            //window.location.reload();
        }

        loadLogs();
    </script>
</body>
</html>