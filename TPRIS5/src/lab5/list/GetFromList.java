package lab5.list;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static jdk.nashorn.internal.objects.Global.println;

@WebServlet("/GetFromList")
public class GetFromList extends HttpServlet {
    private static final long serialVersionUID = 1L;
    List<String> list = ListOperations.getListInstance();

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        String removed = "";
        List<String> oddElements = new ArrayList<>();


        System.out.println(Arrays.toString(list.toArray()));//list.remove(i);
        for (int i = 0; i < list.size(); i++) {
            if (i % 2 != 0) {
                oddElements.add(list.get(i));
             //   String obj = list.get(i);
               // System.out.println(i);
            }

        }

        list.clear();
        list.addAll(oddElements);
        oddElements.clear();
        //list=oddElements;
        removed=String.join(", ",list);
        // some validation code
        out.println(removed);
        out.close();
    }

}