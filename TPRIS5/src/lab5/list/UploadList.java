package lab5.list;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebServlet("/UploadList")
public class UploadList extends HttpServlet {
    private static final long serialVersionUID = 1L;
    List<String> list = ListOperations.getListInstance();

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        list.clear();
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
//        Integer index = Integer.parseInt(req.getParameter("index"));
        String[] data = req.getParameter("data").split(",");
        list.addAll(Arrays.asList(data));
        System.out.println(data);
        // some validation code
      //  out.println("Size"+list.size());
        out.close();
    }

}