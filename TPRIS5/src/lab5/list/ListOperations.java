package lab5.list;

import java.util.LinkedList;
import java.util.List;

public class ListOperations {

    private static List<String> list = new LinkedList<>();

    private ListOperations(){};

    public static List<String> getListInstance(){
        return list;
    }

}
