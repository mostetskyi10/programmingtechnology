package lab5.list;

import com.fasterxml.jackson.databind.ObjectMapper;
import lab5.tree.TreeOperations;
import lab5.tree.Tree;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/SplitTree")
public class SplitTree extends HttpServlet {
    private static final long serialVersionUID = 1L;
    Tree.Node tree = TreeOperations.getTreeInstance();

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        ObjectMapper obj = new ObjectMapper();

        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        // extract the user name and password from req object sent by client
        String element = req.getParameter("split");
        System.out.println(element);
        Tree.Node tree2 = Tree.splitTree(tree,Integer.parseInt(element));
       // tree.print();

        Tree.Node[] treeArray = new Tree.Node[]{tree,tree2};
        // some validation code
        out.println(obj.writeValueAsString(treeArray));
        out.close();
    }

}