package lab5.list;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/InsertIntoList")
public class InsertIntoList extends HttpServlet {
    private static final long serialVersionUID = 1L;
    List<String> list = ListOperations.getListInstance();

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        // extract the user name and password from req object sent by client
        String element = req.getParameter("element");

        list.add(element);

        // some validation code
        out.println("Added :"+element+" List size:"+list.size() );
        out.close();
    }

}