package lab5.tree;

public class TreeOperations {
    private static Tree.Node tree = new Tree.Node(16);

    private TreeOperations(){}

    public static Tree.Node getTreeInstance(){
        TreeOperations.fillTree();
        return tree;
    }

    public static void fillTree(){
        for (int step = 16; step > 1; step /= 2) {
            for (int value = step/2; value < 32; value += step) {
                tree.add(value);
            }
        }
    }
}
