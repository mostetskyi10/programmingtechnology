package lab5.tree;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class Tree {
    public static class Node {
        Integer data = null;
        Node left = null;
        Node right = null;

        public Integer getData() {
            return data;
        }

        public void setData(Integer data) {
            this.data = data;
        }

        public Node getLeft() {
            return left;
        }

        public void setLeft(Node left) {
            this.left = left;
        }

        public Node getRight() {
            return right;
        }

        public void setRight(Node right) {
            this.right = right;
        }

        public Node(Integer value) {
            data = value;
        }

        public void add(Integer value) {
            if (value < data) {
                if (left != null) {
                    left.add(value);
                } else {
                    left = new Node(value);
                }
            } else if (right != null) {
                right.add(value);
            } else {
                right = new Node(value);
            }
        }

        public void print() {
            print("", true);
        }

        private void print(String prefix, boolean isTail) {
            System.out.println(prefix + (isTail ? "--- " : "|-- ") + data);
            String nextPrefix = prefix + (isTail ? "    " : "|   ");
            if (left != null || right != null) {
                if (left != null) {
                    left.print(nextPrefix, right == null);
                } else {
                    System.out.println(nextPrefix + (right == null ? "--- " : "|-- "));
                }
                if (right != null) {
                    right.print(nextPrefix, true);
                } else {
                    System.out.println(nextPrefix + "--- ");
                }
            }
        }


    }

    static void setChild(Node node, boolean toLeft, Node child){
        if (toLeft) {
            node.left = child;
        } else {
            node.right = child;
        }
    }

   public static Node splitTree(Node root, int k){
        Node root2 = null;
        Node parent1 = null;
        Node parent2 = null;
        // Determine at which side of the root we will travel
        boolean toLeft = root != null && k < root.data;

        while (root != null) {
            while (root != null && (k < root.data) == toLeft) {
                parent1 = root;
                root = toLeft ? root.left : root.right;
            }
            setChild(parent1, toLeft, null); // Cut out the edge. root is now detached
            toLeft = !toLeft; // toggle direction
            if (root2 == null) {
                root2 = root; // This is the root of the other tree.
            } else if (parent2 != null) {
                setChild(parent2, toLeft, root); // re-attach the detached subtree
            }
            parent2 = parent1;
            parent1 = null;
        }
        return root2;
    }

    public static void main(String[] args) throws IOException, Exception {
        // Populate tree with some data
        ObjectMapper obj = new ObjectMapper();

        Node root;
        root = new Node(16);
        for (int step = 16; step > 1; step /= 2) {
            for (int value = step/2; value < 32; value += step) {
                root.add(value);
            }
        }
        // Split at some value
        Node root2 = splitTree(root, 2);
        // Output the resulting trees

        Node[] nodes = new Node[]{root,root2};
        System.out.println(      obj.writeValueAsString(nodes));

      //  System.out.println(obj.writeValueAsString(root));
       // System.out.println(obj.writeValueAsString(root2));
        //root.print();
        //root2.print();
    }
}
