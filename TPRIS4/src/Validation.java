import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(
        name = "FormAppEngine",
        urlPatterns = {"/roses"}
)
public class Validation extends HttpServlet
{
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        // extract the user name and password from req object sent by client
        String str1 = req.getParameter("job");
        String str2 = req.getParameter("priority");
        // some validation code
        PriorityQueue.getInstance().insert(str1,Integer.parseInt(str2));
        out.println(PriorityQueue.getInstance().size());
        out.close();
        }

        public int getSize(){
        return PriorityQueue.getInstance().size();
        }
}