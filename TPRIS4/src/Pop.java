import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(
        name = "FormPopAppEngine",
        urlPatterns = {"/pop"}
)
public class Pop extends HttpServlet
{
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        // extract the user name and password from req object sent by client
        // some validation code
        //PriorityQueue.getInstance().remove();
        out.println(PriorityQueue.getInstance().remove());
        out.close();
    }

    public int getSize(){
        return PriorityQueue.getInstance().size();
    }
}