<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>jQuery, Ajax and Servlet/JSP integration example</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"
            type="text/javascript"></script>
    <script src="https://www.gstatic.com/firebasejs/5.0.1/firebase.js"></script>
    <script type="text/javascript">
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyBsLOcrPVHXiw8ggMPzV0W26eQca4MuHIs",
            authDomain: "my-lab2-f6420.firebaseapp.com",
            databaseURL: "https://my-lab2-f6420.firebaseio.com",
            projectId: "my-lab2-f6420",
            storageBucket: "my-lab2-f6420.appspot.com",
            messagingSenderId: "99180894813"
        };
        firebase.initializeApp(config);
    </script>
</head>
<body>

<form>
<div class="form-group">
    Value: <input type="text" id="job"/>
    Priority: <input type="text" id="priority"/>
</div>
    <input type="button" onclick="pushData()" value="Push">
    <input type="button" onclick="popData()" value="Pop">
</form>
<br>
<br>


<strong>Logs:<strong>
<div id="logs">

</div>


<strong>Dequeue:</strong>
<div id="ajaxGetUserServletResponse"></div>

<script>
    $(document).ready(function() {
        $('#userName').blur(function(event) {
            var name = $('#userName').val();
            $.get('GetUserServlet', {
                userName : name
            }, function(responseText) {
                $('#ajaxGetUserServletResponse').text(responseText);
            });
        });
    });


    function sayHello() {
        alert("Hello")
    }

    function pushData() {
        var job = $('#job').val();
        var priority = $('#priority').val();

        $.get('roses', {
            job : job,
            priority: priority
        }, function(responseText) {
            var uid = firebase.database().ref().child('queue').push().key;
            var updates ={};
            updates['/queue/' +job] =" Value added: Priority = " + priority + " Value = " + job;
            firebase.database().ref().update(updates);

          //  $('#logs').append(("<br/><label>Value added: Priority = " + priority + " Value = " + job + "</label>"))
          //  $('#ajaxGetUserServletResponse').text(responseText);

        });
        window.location.reload();

    }

    function popData() {
        $.get('pop',function(responseText) {

            var job=responseText.substring(responseText.indexOf(":")+1,responseText.indexOf("P")).trim();
            firebase.database().ref().child('/queue/' + job).remove();
            //  $('#logs').append(("<br/><label>Value added: Priority = " + priority + " Value = " + job + "</label>"))
            $('#ajaxGetUserServletResponse').text("Value: "+job);
        });
        //window.location.reload();
    }


    function loadLogs() {
        var databaseRef = firebase.database().ref('queue/');

        databaseRef.once('value', function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childData = childSnapshot.val();
                $("#logs").append("<br/>" + childData);
                var job = childData.substring(childData.lastIndexOf("=")+1,childData.length).trim()
                var priority= childData.substring(childData.indexOf("=")+1,childData.lastIndexOf("V")).trim()

                $.get('UploadList', {
                    job : job,
                    priority: priority});
            });


        });
    }
    loadLogs();
</script>
</body>
</html>