package app.model;

public enum MathOperator {
    ADD("+"), SUBTRACT("-"), MULTIPLY("*"), DIVIDE(":"), NO_SIGN("");

    private String sign;

    MathOperator(String sign) {
        this.sign = sign;
    }

    public String getSign() {
        return sign;
    }

    public static MathOperator get(String sign) {
        for (MathOperator mo : MathOperator.values()) {
            if (mo.getSign().equals(sign)) {
                return mo;
            }
        }

        return MathOperator.NO_SIGN;
    }

    public int precedence() {
        switch(this) {
            case ADD:
            case SUBTRACT:
                return 1;
            case MULTIPLY:
            case DIVIDE:
                return 2;
            default:
                return 0;
        }
    }

}
