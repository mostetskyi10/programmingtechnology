package app.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class BTreePrinterTest  {
    String string;

    public  BTreePrinterTest(String s){
        this.string=s;
    }
    private static Node<Integer> test1() {
        Node<Integer> root = new Node<Integer>(2);
        Node<Integer> n11 = new Node<Integer>(7);
        Node<Integer> n12 = new Node<Integer>(5);
        Node<Integer> n21 = new Node<Integer>(2);
        Node<Integer> n22 = new Node<Integer>(6);
        Node<Integer> n23 = new Node<Integer>(3);
        Node<Integer> n24 = new Node<Integer>(6);
        Node<Integer> n31 = new Node<Integer>(5);
        Node<Integer> n32 = new Node<Integer>(8);
        Node<Integer> n33 = new Node<Integer>(4);
        Node<Integer> n34 = new Node<Integer>(5);
        Node<Integer> n35 = new Node<Integer>(8);
        Node<Integer> n36 = new Node<Integer>(4);
        Node<Integer> n37 = new Node<Integer>(5);
        Node<Integer> n38 = new Node<Integer>(8);

        root.left = n11;
        root.right = n12;

        n11.left = n21;
        n11.right = n22;
        n12.left = n23;
        n12.right = n24;

        n21.left = n31;
        n21.right = n32;
        n22.left = n33;
        n22.right = n34;
        n23.left = n35;
        n23.right = n36;
        n24.left = n37;
        n24.right = n38;

        return root;
    }

    private static Node<Integer> test2() {
        Node<Integer> root = new Node<Integer>(2);
        Node<Integer> n11 = new Node<Integer>(7);
        Node<Integer> n12 = new Node<Integer>(5);
        Node<Integer> n21 = new Node<Integer>(2);
        Node<Integer> n22 = new Node<Integer>(6);
        Node<Integer> n23 = new Node<Integer>(9);
        Node<Integer> n31 = new Node<Integer>(5);
        Node<Integer> n32 = new Node<Integer>(8);
        Node<Integer> n33 = new Node<Integer>(4);

        root.left = n11;
        root.right = n12;

        n11.left = n21;
        n11.right = n22;

        n12.right = n23;
        n22.left = n31;
        n22.right = n32;

        n23.left = n33;

        return root;
    }

    private static Node<Character> test3() {
        Node<Character> root = new Node<Character>('v');
        Node<Character> n11 = new Node<Character>('l');
        Node<Character> n12 = new Node<Character>('e');
        Node<Character> n21 = new Node<Character>('e');
        Node<Character> n22 = new Node<Character>('l');


        root.left = n12;
        root.right = n21;

        n12.left = n11;
        n21.right = n22;

        BTreePrinter.goLeftRoot(root);

        return root;
    }

    public static Node<Character> test4(String line) {
        String str =line;
        Node<Character> root;
        if(str.length()%2!=0) {
            int i =(str.length()/2)-1;
            int k =0,j =str.length();
             root = new Node<Character>(str.charAt(str.length()/2));
            Node<Character> temp = root;
            Node<Character> temp2=root;
            while(k <= i){
                temp.left=new Node<Character>(str.charAt(i));
                temp= temp.left;
                i--;
            }
            int length = (str.length()/2)+1;
            while (length<j){
                temp2.right=new Node<Character>(str.charAt(length));
                temp2= temp2.right;
                length++;
            }
       }
       else {
            int i =(str.length()/2)-1;
            int k =0,j =str.length();
            root = new Node<Character>(' ');
            Node<Character> temp = root;
            Node<Character> temp2=root;
            while(k <= i){
                temp.left=new Node<Character>(str.charAt(i));
                temp= temp.left;
                i--;
            }
            int length = (str.length()/2);
            while (length<j){
                temp2.right=new Node<Character>(str.charAt(length));
                temp2= temp2.right;
                length++;
            }
        }


        BTreePrinter.goLeftRoot(root);
        BTreePrinter.goRightRoot(root);

        return root;
    }



    public static void main(String[] args) throws InterruptedException {


        BTreePrinter thread1 = new BTreePrinter("test1");
        BTreePrinter thread2 = new BTreePrinter("test2");
        BTreePrinter thread3 = new BTreePrinter("test3");


        thread1.start();
        thread2.start();
        thread3.start();

        thread1.join();
        thread2.join();
        thread3.join();

        //new BTreePrinter(BTreePrinterTest.test4("test")).start();
     //   BTreePrinter.printNode(test4("testset"));
      //  BTreePrinter.printNode(test2());

    }
}

 class Node<T extends Comparable<?>> {
   public Node<T> left, right;
   public T data;

    public Node(T data) {
        this.data = data;
    }
}



class BTreePrinter extends Thread{
String line;

public BTreePrinter(String line){
    this.line=line;
}

    public static <T extends Comparable<?>> void printNode(Node<T> root) {
        int maxLevel = BTreePrinter.maxLevel(root);

        printNodeInternal(Collections.singletonList(root), 1, maxLevel);
    }

    private static <T extends Comparable<?>> void printNodeInternal(List<Node<T>> nodes, int level, int maxLevel) {
        if (nodes.isEmpty() || BTreePrinter.isAllElementsNull(nodes))
            return;

        int floor = maxLevel - level;
        int endgeLines = (int) Math.pow(2, (Math.max(floor - 1, 0)));
        int firstSpaces = (int) Math.pow(2, (floor)) - 1;
        int betweenSpaces = (int) Math.pow(2, (floor + 1)) - 1;

        BTreePrinter.printWhitespaces(firstSpaces);

        List<Node<T>> newNodes = new ArrayList<Node<T>>();
        for (Node<T> node : nodes) {
            if (node != null) {
                System.out.print(node.data);
                newNodes.add(node.left);
                newNodes.add(node.right);
            } else {
                newNodes.add(null);
                newNodes.add(null);
                System.out.print(" ");
            }

            BTreePrinter.printWhitespaces(betweenSpaces);
        }
        System.out.println("");

        for (int i = 1; i <= endgeLines; i++) {
            for (int j = 0; j < nodes.size(); j++) {
                BTreePrinter.printWhitespaces(firstSpaces - i);
                if (nodes.get(j) == null) {
                    BTreePrinter.printWhitespaces(endgeLines + endgeLines + i + 1);
                    continue;
                }

                if (nodes.get(j).left != null)
                    System.out.print("/");
                else
                    BTreePrinter.printWhitespaces(1);

                BTreePrinter.printWhitespaces(i + i - 1);

                if (nodes.get(j).right != null)
                    System.out.print("\\");
                else
                    BTreePrinter.printWhitespaces(1);

                BTreePrinter.printWhitespaces(endgeLines + endgeLines - i);
            }

            System.out.println("");
        }

        printNodeInternal(newNodes, level + 1, maxLevel);
    }

    private static void printWhitespaces(int count) {
        for (int i = 0; i < count; i++)
            System.out.print(" ");
    }

    private static <T extends Comparable<?>> int maxLevel(Node<T> node) {
        if (node == null)
            return 0;

        return Math.max(BTreePrinter.maxLevel(node.left), BTreePrinter.maxLevel(node.right)) + 1;
    }

    private static <T> boolean isAllElementsNull(List<T> list) {
        for (Object object : list) {
            if (object != null)
                return false;
        }

        return true;
    }

    public static void goLeftRoot(Node root) {
        System.out.print("LeftRootRight: ");
        goLeftRootRight(root);
        System.out.println();
    }

    private static void goLeftRootRight(Node node) {
        if (node != null) {
            goLeftRootRight(node.left);
            System.out.print(node.data + " ");
            goLeftRootRight(node.right);
        }
    }

    public static void goRightRoot(Node root) {
        System.out.print("RightRootLeft: ");
        goRightRootRight(root);
        System.out.println();
    }

    private static void goRightRootRight(Node node) {
        String res ="";
        if (node != null) {
            goRightRootRight(node.right);
            System.out.print(node.data + " ");
            goRightRootRight(node.left);
        }

    }
@Override
    public void run(){
    Random r = new Random();
    int low = 1000;
    int high = 3000;
    int result = r.nextInt(high-low) + low;
    try {
        Thread.sleep(result);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    printNode(BTreePrinterTest.test4(line));
}
}
